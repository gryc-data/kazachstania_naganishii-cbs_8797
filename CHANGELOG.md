# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.1 (2021-04-28)

### Edited

* Build all locus hierarchy (e.g., gene > mRNA > CDS).

### Fixed

* Coding gene KNAG_0B04080 turned into pseudo (interupted by a gap).
* Coding gene KNAG_0A02300 turned into pseudo (interupted by a gap).
* Coding gene KNAG_0A05330 turned into pseudo (interupted by a gap).
* Coding gene KNAG_0K01643 turned into pseudo (interupted by a gap).
* Coding gene KNAG_0A08040 turned into pseudo (interupted by scaffold boundary).
* Coding gene KNAG_0D00100 turned into pseudo (interupted by scaffold boundary).
* Coding gene KNAG_0D05400 turned into pseudo (interupted by scaffold boundary).
* Coding gene KNAG_0E04280 turned into pseudo (interupted by scaffold boundary).
* Coding gene KNAG_0F04040 turned into pseudo (interupted by scaffold boundary).
* Coding gene KNAG_0G00100 turned into pseudo (interupted by scaffold boundary).

## v1.0 (2021-04-28)

### Added

* The 13 annotated chromosomes of Kazachstania naganishii CBS 8797 (source EBI, [GCA_000348985.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000348985.1)).

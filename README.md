## *Kazachstania naganishii* CBS 8797

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEA70969](https://www.ebi.ac.uk/ena/browser/view/PRJEA70969)
* **Assembly accession**: [GCA_000348985.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000348985.1)
* **Original submitter**: Wolfe Laboratory (Trinity College Dublin)

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: ASM34898v1
* **Assembly length**: 10,845,821
* **#Chromosomes**: 13
* **Mitochondiral**: No
* **N50 (L50)**: 856,010 (5)

### Annotation overview

* **Original annotator**: Wolfe Laboratory (Trinity College Dublin)
* **CDS count**: 5321
* **Pseudogene count**: 0
* **tRNA count**: 159
* **rRNA count**: 8
* **Mobile element count**: 0
